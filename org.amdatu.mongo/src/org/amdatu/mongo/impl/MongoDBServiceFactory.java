/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.impl;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

public class MongoDBServiceFactory implements ManagedServiceFactory {
    private final Map<String, Component> m_Components = new ConcurrentHashMap<String, Component>();
    private volatile LogService m_logService;
    private volatile DependencyManager m_dependencyManager;
    
    @Override
    public String getName() {
        return "org.amdatu.mongo";
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void updated(String pid, Dictionary properties) throws ConfigurationException {
        if (!m_Components.containsKey(pid)) {
            String host = getProperty(properties, "host", "localhost");
            int port = getProperty(properties, "port", 27017);
            String mongoURI = getProperty(properties, "mongoURI", null);
            String dbName = getProperty(properties, "dbName", "test");
            String username = getProperty(properties, "username", null);
            String password = getProperty(properties, "password", null);
            
            try {
                MongoClient mongo;
                if (mongoURI != null) {
                	MongoClientURI uri = new MongoClientURI(mongoURI);
                    mongo = new MongoClient(uri);
                }
                else {
                    MongoClientOptions mongoOptions = createMongoOptions(properties);
                    
                    mongo = createMongo(host, port, mongoOptions);
                }

                if(username != null) {
                    mongo.getDB(dbName).authenticate(username, password.toCharArray());
                    m_logService.log(LogService.LOG_INFO, "Authenticated as '" + username + "'");
                }
                
                MongoDBServiceImpl instance = new MongoDBServiceImpl(mongo, mongo.getDB(dbName));
                
                Properties serviceProperties = new Properties();
                serviceProperties.put("dbName", dbName);
                Component component = m_dependencyManager.createComponent().setInterface(MongoDBService.class.getName(), serviceProperties).setImplementation(instance);
                m_dependencyManager.add(component);
                m_Components.put(pid, component);
                m_logService.log(LogService.LOG_INFO, "Created MongoDB service for pid '" + pid + "'");
                
            }

            catch (MongoException e) {
                throw new RuntimeException(e);
            }
            catch (UnknownHostException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private MongoClient createMongo(String host, int port, MongoClientOptions mongoOptions) throws UnknownHostException {
        MongoClient mongo;
        if(host.contains(",")) {
            String[] hosts = host.split(",");
            List<ServerAddress> addresses = new ArrayList<ServerAddress>();
            for (String hostUrl : hosts) {
                ServerAddress serverAddress;
                if(hostUrl.contains(":")) {
                    String[] hostUrlParts = hostUrl.split(":");
                    port = Integer.parseInt(hostUrlParts[1]);
                    serverAddress = new ServerAddress(hostUrlParts[0], port);
                } else {
                    serverAddress = new ServerAddress(hostUrl);
                }
                
                addresses.add(serverAddress);
            }
            
            mongo = new MongoClient(addresses, mongoOptions);
        } else {
            mongo = new MongoClient(new ServerAddress(host, port), mongoOptions);
        }
        return mongo;
    }

    private MongoClientOptions createMongoOptions(Dictionary<String,String> properties) throws ConfigurationException {
    	boolean autoConnectRetry = getProperty(properties, "autoConnectRetry", false);
        int connectionsPerHost = getProperty(properties, "connectionsPerHost", 100);
        int connectTimeout = getProperty(properties, "connectTimeout", 10000);
        boolean cursorFinalizerEnabled = getProperty(properties, "cursorFinalizerEnabled", true);
        String description = getProperty(properties, "description", "");
        long maxAutoConnectRetryTime = getProperty(properties, "maxAutoConnectRetryTime", 0l);
        int maxWaitTime = getProperty(properties, "maxWaitTime", 120000);
        boolean socketKeepAlive  = getProperty(properties, "socketKeepAlive ", false);
        int socketTimeout = getProperty(properties, "socketTimeout", 0);
        int threadsAllowedToBlockForConnectionMultiplier = getProperty(properties, "threadsAllowedToBlockForConnectionMultiplier", 5);
        String readPreference = getProperty(properties, "readPreference", "primary");
        String writeConcern = getProperty(properties, "writeConcern", "ACKNOWLEDGED");
        
        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        
        builder.autoConnectRetry(autoConnectRetry);
        builder.connectionsPerHost(connectionsPerHost);
        builder.connectTimeout(connectTimeout);
        builder.cursorFinalizerEnabled(cursorFinalizerEnabled);
        builder.description(description);
        builder.maxAutoConnectRetryTime(maxAutoConnectRetryTime);
        builder.maxWaitTime(maxWaitTime);
        builder.socketKeepAlive(socketKeepAlive);
        builder.socketTimeout(socketTimeout);
        builder.threadsAllowedToBlockForConnectionMultiplier(threadsAllowedToBlockForConnectionMultiplier);
        
        setReadPreference(readPreference, builder);
        setWriteConcern(writeConcern, builder);
        
        return builder.build();
    }

	private void setWriteConcern(String writePreference, MongoClientOptions.Builder builder) throws ConfigurationException {
		
		if("ACKNOWLEDGED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.ACKNOWLEDGED);
        } else if("ERRORS_IGNORED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.ERRORS_IGNORED);
        } else if("FSYNC_SAFE".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.FSYNC_SAFE);
        } else if("FSYNCED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.FSYNCED);
        } else if("JOURNAL_SAFE".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.JOURNAL_SAFE);
        } else if("JOURNALED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.JOURNALED);
        } else if("MAJORITY".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.MAJORITY);
        } else if("NONE".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.NONE);
        } else if("NORMAL".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.NORMAL);
        } else if("REPLICA_ACKNOWLEDGED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.REPLICA_ACKNOWLEDGED);
        } else if("REPLICAS_SAFE".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.REPLICAS_SAFE);
        } else if("SAFE".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.SAFE);
        } else if("UNACKNOWLEDGED".equalsIgnoreCase(writePreference)) {
        	builder.writeConcern(WriteConcern.UNACKNOWLEDGED);
        } else {
        	throw new ConfigurationException("writeConcern", "Invalid writeConcern value");
        }
	}

	private void setReadPreference(String readPreference,
			MongoClientOptions.Builder builder) throws ConfigurationException {
		if("primary".equals(readPreference)) {
        	builder.readPreference(ReadPreference.primary());
        } else if("primaryPreferred".equals(readPreference)) {
        	builder.readPreference(ReadPreference.primaryPreferred());
        } else if("secondary".equals(readPreference)) {
        	builder.readPreference(ReadPreference.secondary());
        } else if("secondaryPreferred".equals(readPreference)) {
        	builder.readPreference(ReadPreference.secondaryPreferred());
        } else if("nearest".equals(readPreference)) {
        	builder.readPreference(ReadPreference.nearest());
        } else {
        	throw new ConfigurationException("readPreference", "Invalid value for readPreference");
        }
	}

    @Override
    public void deleted(String pid) {
        if(m_Components.containsKey(pid)) {
            Component instance = m_Components.get(pid);
            ((MongoDBServiceImpl)instance.getService()).close();
            
            //Potentially removed by a concurrent thread.
            if(instance != null) {
            	m_dependencyManager.remove(instance);
            }
            
            m_Components.remove(pid);
            
            m_logService.log(LogService.LOG_INFO, "Removed MongoDB service for pid '" + pid + "'");
        }
    }

    private String getProperty(Dictionary<String, String> properties, String key, String defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return value;
        }
    }
    
    private boolean getProperty(Dictionary<String, String> properties, String key, boolean defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return Boolean.parseBoolean(value);
        }
    }
    
    private int getProperty(Dictionary<String, String> properties, String key, int defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return Integer.parseInt(value);
        }
    }
    
    private long getProperty(Dictionary<String, String> properties, String key, long defaultValue) {
        String value = properties.get(key);
        if (value == null) {
            return defaultValue;
        }
        else {
            return Long.parseLong(value);
        }
    }

}
