/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import java.util.concurrent.TimeUnit;

import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.mongo.MongoDBService;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

public class DefaultMongoDbServiceTest extends BaseOSGiServiceTest<MongoDBService> {
	
	private String mongopid;
	private static final String COLLECTION = "demo";
	private DBCollection m_collection;
	
	public DefaultMongoDbServiceTest() {
		super(MongoDBService.class);
	}
	
	@Override
	public void setUp() throws Exception {
		mongopid = configureFactory("org.amdatu.mongo", properties());
		super.setUp();
        // Check if the configured collection is actually available
		m_collection = MongoTestUtil.getDBCollection(instance, COLLECTION);
	}
	
	public void tearDown() throws Exception{
		removeConfig(mongopid);
		super.tearDown();
	}
	
	public void testDefaultMongoDbService() throws Exception {
		DB db = instance.getDB();
		assertNotNull(db);
		assertEquals("test", db.getName());
		assertEquals(WriteConcern.ACKNOWLEDGED, db.getWriteConcern());
		assertEquals(ReadPreference.primary(), db.getReadPreference());
	}
	
	public void testCleanupConnections() throws InterruptedException {
		if (!canRunTest()) {
			return;
		}
		int nrOfConnections = getNrOfConnections();
		
		for(int i = 0; i < 10; i++) {
			DB db = instance.getDB();
			assertEquals("test", db.getName());
			db.getCollection(COLLECTION).save(new BasicDBObject("name", i));
			stopBundle("org.amdatu.mongo");
			startBundle("org.amdatu.mongo");
			
			TimeUnit.MILLISECONDS.sleep(500);
		}
		
		assertEquals(nrOfConnections, getNrOfConnections());
	}
	
	private int getNrOfConnections() {
		DB db = instance.getDB();
		CommandResult status = db.command("serverStatus");
		DBObject currentConnections = (DBObject)status.get("connections");
		return (Integer)currentConnections.get("current");
	}
	
    private boolean canRunTest() {
        return m_collection != null;
    }
	
}
