/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.itest;

import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.mongo.MongoDBService;

import com.mongodb.DB;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;


public class ConfiguredMongoDbServiceTest extends BaseOSGiServiceTest<MongoDBService> {
	private String mongopid;
	
	public ConfiguredMongoDbServiceTest() {
		super(MongoDBService.class);
	}
	
	@Override
	public void setUp() throws Exception {
		mongopid = configureFactory("org.amdatu.mongo", properties("dbName", "mydb1", 
				"readPreference", "secondaryPreferred", 
				"writeConcern", "FSYNCED"));
		super.setUp();
	}
	
	public void testConfiguredMongoDbService() throws Exception {
		DB db = instance.getDB();
		assertNotNull(db);
		assertEquals("mydb1", db.getName());
		assertEquals(ReadPreference.secondaryPreferred(), db.getReadPreference());
		assertEquals(WriteConcern.FSYNCED, db.getWriteConcern());
	}
	
	@Override
	protected void tearDown() throws Exception {
		removeConfig(mongopid);
		super.tearDown();
	}
}
